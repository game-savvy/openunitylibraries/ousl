using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;
using GameSavvy.OpenUnityTools.Attributes;

namespace GameSavvy.OUSL.Variables
{
    public class ColorDataAttributeDrawer : OdinAttributeDrawer<ColorDataAttribute, Color>
    {
        override
        protected void DrawPropertyLayout(GUIContent label)
        {
            SirenixEditorGUI.BeginBox();
            {


                var rect = EditorGUILayout.GetControlRect();

                var value = ValueEntry.SmartValue;
                string hexCode = ColorUtility.ToHtmlStringRGB(value);

                if (label != null) rect = EditorGUI.PrefixLabel(rect, label);

                rect = EditorGUILayout.GetControlRect();
                value = SirenixEditorFields.ColorField(rect.AlignLeft(rect.width * 0.75f - 4f), value);

                // HEX
                hexCode = SirenixEditorFields.TextField(rect.AlignRight(rect.width * 0.25f - 4f), $"#{hexCode}");
                if (ColorUtility.TryParseHtmlString(hexCode, out value))
                {
                    ValueEntry.SmartValue = value;
                }

                // RGB
                rect = EditorGUILayout.GetControlRect();
                GUIHelper.PushLabelWidth(15);
                value.r = EditorGUI.Slider(rect.AlignLeft(rect.width * 0.3f), "R", value.r, 0f, 1f);
                value.g = EditorGUI.Slider(rect.AlignCenter(rect.width * 0.3f), "G", value.g, 0f, 1f);
                value.b = EditorGUI.Slider(rect.AlignRight(rect.width * 0.3f), "B", value.b, 0f, 1f);
                GUIHelper.PopLabelWidth();

                // HSV
                Color.RGBToHSV(value, out var h, out var s, out var v);
                rect = EditorGUILayout.GetControlRect();
                GUIHelper.PushLabelWidth(15);
                h = EditorGUI.Slider(rect.AlignLeft(rect.width * 0.3f), "H", h, 0f, 1f);
                s = EditorGUI.Slider(rect.AlignCenter(rect.width * 0.3f), "S", s, 0f, 1f);
                v = EditorGUI.Slider(rect.AlignRight(rect.width * 0.3f), "V", v, 0f, 1f);
                GUIHelper.PopLabelWidth();
                value = Color.HSVToRGB(h, s, v);

                ValueEntry.SmartValue = value;
            }
            SirenixEditorGUI.EndBox();
        }
    }
}
