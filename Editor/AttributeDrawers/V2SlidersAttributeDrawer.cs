using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;
using GameSavvy.OpenUnityTools.Attributes;

namespace GameSavvy.OUSL.Variables
{
    public class V2SlidersAttributeDrawer : OdinAttributeDrawer<V2SlidersAttribute, Vector2>
    {
        override
        protected void DrawPropertyLayout(GUIContent label)
        {
            var rect = EditorGUILayout.GetControlRect();

            if (label != null) rect = EditorGUI.PrefixLabel(rect, label);

            var value = ValueEntry.SmartValue;

            GUIHelper.PushLabelWidth(20);
            {
                value.x = EditorGUI.Slider(
                            rect.AlignLeft(rect.width * 0.5f - 4),
                            "X",
                            value.x,
                            Attribute.MinValue,
                            Attribute.MaxValue
                        );
                value.y = EditorGUI.Slider(
                            rect.AlignRight(rect.width * 0.5f - 4),
                            "Y",
                            value.y,
                            Attribute.MinValue,
                            Attribute.MaxValue
                        );
            }
            GUIHelper.PopLabelWidth();

            ValueEntry.SmartValue = value;
        }
    }
}
