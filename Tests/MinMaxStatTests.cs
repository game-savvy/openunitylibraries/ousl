using NUnit.Framework;
using NUAssert = NUnit.Framework.Assert;
using Assert = UnityEngine.Assertions.Assert;

namespace GameSavvy.OUSL.StatSystem.Tests.EditMode
{
    public class _02_MinMaxStatTests
    {
        [Test]
        public void _01_Exists_Yes()
        {
            var stats = new MinMaxStat();
            NUAssert.NotNull(stats);
        }

        [Test]
        public void _02_IsStatRange_Yes()
        {
            var stats = new MinMaxStat();
            NUAssert.IsInstanceOf<MinMaxStat>(stats);
            NUAssert.IsInstanceOf<IStat>(stats);
        }

        [Test]
        public void _03_New_Values()
        {
            var stats = new MinMaxStat();
            Assert.AreApproximatelyEqual(100f, stats.Value);
            Assert.AreApproximatelyEqual(0f, stats.MinValue);
            Assert.AreApproximatelyEqual(100f, stats.MaxValue);
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
        }

        [TestCase(0, 0f, 0, 123, 0f, 1f)]
        [TestCase(123f, 0f, 123f, 456f, 0f, 1f)]
        [TestCase(50f, 50f, 0f, 100f, 1f, 1f)]
        [TestCase(0f, -50f, 0f, 100f, 1f, 1f)]
        [TestCase(-50f, -50f, -100f, 100f, 0f, 1f)]
        public void _04_New_Cases(float expected, float val, float min, float max, float plusMod, float multMod)
        {
            var stats = new MinMaxStat(val, min, max, plusMod, multMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
            Assert.AreApproximatelyEqual(min, stats.MinValue);
            Assert.AreApproximatelyEqual((max + plusMod) * multMod, stats.MaxValue);
            Assert.AreApproximatelyEqual(plusMod, stats.PlusMod);
            Assert.AreApproximatelyEqual(multMod, stats.MultMod);
        }

        [TestCase(100f, 100f, 999f, 0f)]
        [TestCase(120f, 120f, 100f, 20f)]
        [TestCase(150f, 200f, 50f, 100f)]
        [TestCase(50f, 90f, 50f, -10f)]
        [TestCase(20f, 20f, 50f, -80f)]
        [TestCase(0f, 0f, 50f, -800f)]
        public void _05_SetPlusMod_Cases(float expected, float expectedMax, float val, float plusMod)
        {
            var stats = new MinMaxStat(val);
            stats.SetPlusMod(plusMod);
            Assert.AreApproximatelyEqual(expectedMax, stats.MaxValue);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(130f, 130f, 999f, 10f, 20f)]
        [TestCase(60f, 130f, 30, 10f, 20f)]
        [TestCase(30f, 70f, 30, -10f, -20f)]
        [TestCase(10f, 10f, 30, -50f, -40f)]
        [TestCase(30f, 100f, 30, 0f, 0f)]
        public void _06_AddPlusMod_Cases(float expected, float expectedMax, float val, float plusMod1, float plusMod2)
        {
            var stats = new MinMaxStat(val);
            stats.AddPlusMod(plusMod1);
            stats.AddPlusMod(plusMod2);
            Assert.AreApproximatelyEqual(expectedMax, stats.MaxValue);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(0f, 0f, 50f, 0f)]
        [TestCase(0f, 0f, 50f, -1f)]
        [TestCase(50f, 100f, 50f, 1f)]
        [TestCase(150f, 200f, 50f, 2f)]
        [TestCase(50f, 70f, 50f, 0.7f)]
        [TestCase(30f, 30f, 50f, 0.3f)]
        public void _07_SetMultMod_Cases(float expected, float expectedMax, float val, float multMod)
        {
            var stats = new MinMaxStat(val);
            stats.SetMultMod(multMod);
            Assert.AreApproximatelyEqual(expectedMax, stats.MaxValue);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(50f, 100f, 50f, 0f, 0f)]
        [TestCase(0f, 0f, 50f, -1f, 0f)]
        [TestCase(0f, 0f, 50f, 0f, -1f)]
        [TestCase(80f, 130f, 50f, 0.1f, 0.2f)]
        [TestCase(50f, 70f, 50f, -0.1f, -0.2f)]
        [TestCase(30f, 30f, 50f, -0.3f, -0.4f)]
        [TestCase(0f, 0f, 50f, -1f, -2f)]
        public void _08_AddMultMod_Cases(float expected, float expectedMax, float val, float multMod1, float multMod2)
        {
            var stats = new MinMaxStat(val);
            stats.AddMultMod(multMod1);
            stats.AddMultMod(multMod2);
            Assert.AreApproximatelyEqual(expectedMax, stats.MaxValue);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(100f, 100f, 0f, 1f)]
        [TestCase(100f, 100f, 2f, 3f)]
        [TestCase(100f, 200f, -2f, -3f)]
        [TestCase(50f, 50, 0f, 1f)]
        [TestCase(100f, 50f, 0f, 0f)]
        public void _09_ResetMods_Cases(float expected, float val, float plusMod, float multMod)
        {
            var stats = new MinMaxStat(val, 0f, 100f, plusMod, multMod);
            stats.ResetMods();
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
            Assert.AreApproximatelyEqual(100f, stats.MaxValue);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(100f, 100f, 50f, 3f, 100f)]
        [TestCase(50f, 50, 50f, 3f, 150f)]
        [TestCase(100f, 50, 0f, 1f, 150f)]
        [TestCase(20f, 50, 50f, 2f, 20f)]
        public void _10_ResetTo_Cases(float expected, float val, float plusMod, float multMod, float newMax)
        {
            var stats = new MinMaxStat(val, 0f, 100f, plusMod, multMod);
            stats.ResetMaxTo(newMax);
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
            Assert.AreApproximatelyEqual(newMax, stats.MaxValue);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(50f, 50f, 0f, 100f, 50f)]
        [TestCase(50f, 50f, 0f, 50f, 220f)]
        [TestCase(0f, -50f, 0f, 50f, -50f)]
        public void _11_SetMinValue_Cases(float expectedVal, float expectedMin, float initialMin, float initialMax, float newMin)
        {
            var stats = new MinMaxStat(initialMin, initialMin, initialMax);
            stats.SetMinValue(newMin);
            Assert.AreApproximatelyEqual(expectedMin, stats.MinValue);
            Assert.AreApproximatelyEqual(expectedVal, stats.Value);
        }

        [TestCase(0f, -50f, 0f, 100f)]
        [TestCase(20f, -50f, 20f, 100f)]
        [TestCase(100f, 250f, 0f, 100f)]
        [TestCase(80f, 250f, 0f, 80f)]
        [TestCase(50f, 50f, -100f, 100f)]
        [TestCase(-50f, -50f, -100f, 100f)]
        [TestCase(-100f, -500f, -100f, 100f)]
        public void _12_SetCurrentValue_Cases(float expectedVal, float newValue, float initialMin, float initialMax)
        {
            var stats = new MinMaxStat(0f, initialMin, initialMax);
            stats.Value = newValue;
            Assert.AreApproximatelyEqual(expectedVal, stats.Value);
        }

        [TestCase(1f, 100f, 0f, 100f)]
        [TestCase(0f, 0f, 0f, 100f)]
        [TestCase(0.5f, 50f, 0f, 100f)]
        [TestCase(0.5f, 0f, -100f, 100f)]
        [TestCase(0.25f, -50f, -100f, 100f)]
        [TestCase(0.75f, 50f, -100f, 100f)]
        public void _13_Percent_Cases(float expectedPercent, float value, float min, float max)
        {
            var stats = new MinMaxStat(value, min, max);
            Assert.AreApproximatelyEqual(expectedPercent, stats.Percent);
        }

        [Test]
        public void _14_OnChange_Value_PerformsCallback()
        {
            var stats = new MinMaxStat();
            var called = false;

            stats.OnChange += (v, d) => called = true;
            stats.Value = 50f;

            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(50f, stats.Value);
        }

        [Test]
        public void _15_OnChange_SetPlusMod_PerformsCallback()
        {
            var stats = new MinMaxStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.SetPlusMod(-10f);

            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _16_OnChange_SetMultMod_PerformsCallback()
        {
            var stats = new MinMaxStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.SetMultMod(0.9f);

            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _17_OnChange_AddPlusMod_PerformsCallback()
        {
            var stats = new MinMaxStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.AddPlusMod(-10f);

            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _18_OnChange_AddMultMod_PerformsCallback()
        {
            var stats = new MinMaxStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.AddMultMod(-0.1f);

            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _19_OnChange_ResetMods_PerformsCallback()
        {
            var stats = new MinMaxStat(100f, 0f, 150f, -10f, 0.9f);
            var called = false;
            float newVal = 0f;
            float delta = 0f;
            float expectedDelta = 150f - ((150f - 10f) * 0.9f);

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.ResetMods();

            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(100f + expectedDelta, newVal);
            Assert.AreApproximatelyEqual(expectedDelta, delta);
        }
    }
}