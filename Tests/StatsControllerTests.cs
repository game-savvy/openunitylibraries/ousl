using NUnit.Framework;
using NUAssert = NUnit.Framework.Assert;
using Assert = UnityEngine.Assertions.Assert;

namespace GameSavvy.OUSL.StatSystem.Tests.EditMode
{
    public class _03_StatsControllerTests
    {
        [Test]
        public void _01_Exists_Yes()
        {
            var stats = new StatsController<FakeStatType>();

            NUAssert.NotNull(stats);
        }

        [Test]
        public void _02_IsStatsController_Yes()
        {
            var stats = new StatsController<FakeStatType>();

            NUAssert.IsInstanceOf<StatsController<FakeStatType>>(stats);
            NUAssert.IsInstanceOf<IStatsController<FakeStatType>>(stats);
        }

        [Test]
        public void _03_NewStatsController_HasNoStats()
        {
            var stats = new StatsController<FakeStatType>();

            Assert.AreEqual(0, stats.SimpleStatsCount);
            Assert.AreEqual(0, stats.MinMaxStatsCount);
            Assert.AreEqual(0, stats.StatsCount);
        }

        [Test]
        public void _04_TryAddStat_SimpleStat_CanAdd()
        {
            var stats = new StatsController<FakeStatType>();
            var stat = new SimpleStat();

            var result = stats.TryAddStat(FakeStatType.Speed, stat);

            Assert.IsTrue(result);
            Assert.AreEqual(1, stats.SimpleStatsCount);
        }

        [Test]
        public void _05_TryAddStat_MinMaxStat_CanAdd()
        {
            var stats = new StatsController<FakeStatType>();
            var stat = new MinMaxStat();

            var result = stats.TryAddStat(FakeStatType.Health, stat);

            Assert.IsTrue(result);
            Assert.AreEqual(1, stats.MinMaxStatsCount);
        }

        [Test]
        public void _06_TryAddSimpleStat_CanAdd()
        {
            var stats = new StatsController<FakeStatType>();

            var result = stats.TryAddSimpleStat(FakeStatType.PhysicalDamage, 12f);

            Assert.IsTrue(result);
            Assert.AreEqual(1, stats.SimpleStatsCount);
        }

        [Test]
        public void _07_TryAddMinMaxStat_CanAdd()
        {
            var stats = new StatsController<FakeStatType>();

            var result = stats.TryAddMinMaxStat(FakeStatType.Mana, 80f);

            Assert.IsTrue(result);
            Assert.AreEqual(1, stats.MinMaxStatsCount);
        }

        [TestCase(50f)]
        [TestCase(-50f)]
        public void _08_TryGetSimpleStat_GetsCorrectValue(float value)
        {
            var stats = new StatsController<FakeStatType>();
            stats.TryAddSimpleStat(FakeStatType.PhysicalDamage, value);

            var result = stats.TryGetSimpleStat(FakeStatType.PhysicalDamage, out var stat);

            Assert.IsTrue(result);
            Assert.AreEqual(value, stat.Value);
        }

        [TestCase(50f, 50f, 0f, 100f)]
        [TestCase(0f, -50f, 0f, 50f)]
        [TestCase(150f, 200f, 0f, 150f)]
        public void _09_TryGetMinMaxStat_GetsCorrectValue(float expectedValue, float value, float min, float max)
        {
            var stats = new StatsController<FakeStatType>();
            stats.TryAddMinMaxStat(FakeStatType.Mana, value, min, max);

            var result = stats.TryGetMinMaxStat(FakeStatType.Mana, out var stat);

            Assert.IsTrue(result);
            Assert.AreEqual(expectedValue, stat.Value);
        }

        [TestCase(20f, 20f)]
        [TestCase(0f, -20f)]
        [TestCase(100f, 220f)]
        public void _10_TryGetValue_GetsCorrectValue(float expectedValue, float value)
        {
            var stats = new StatsController<FakeStatType>();
            stats.TryAddMinMaxStat(FakeStatType.Mana, value);
            stats.TryAddSimpleStat(FakeStatType.PhysicalDamage, value);
            float actualValue = 0f;
            bool result = false;

            result = stats.TryGetValue(FakeStatType.PhysicalDamage, out actualValue);
            Assert.IsTrue(result);
            Assert.AreEqual(value, actualValue);

            result = stats.TryGetValue(FakeStatType.Mana, out actualValue);
            Assert.IsTrue(result);
            Assert.AreEqual(expectedValue, actualValue);
        }

        [Test]
        public void _11_TryAddSimpleStat_CanAddMultiple()
        {
            var stats = new StatsController<FakeStatType>();
            Add3SimpleStats(stats);

            Assert.AreEqual(3, stats.StatsCount);
            Assert.AreEqual(3, stats.SimpleStatsCount);
            Assert.AreEqual(0, stats.MinMaxStatsCount);
        }

        [Test]
        public void _12_TryAddMinMaxStat_CanAddMultiple()
        {
            var stats = new StatsController<FakeStatType>();
            Add3MinMaxStats(stats);

            Assert.AreEqual(3, stats.StatsCount);
            Assert.AreEqual(0, stats.SimpleStatsCount);
            Assert.AreEqual(3, stats.MinMaxStatsCount);
        }

        [Test]
        public void _13_TryAddMultipleStats_CanAddMultiple()
        {
            var stats = new StatsController<FakeStatType>();
            Add3MinMaxStats(stats);
            Add3SimpleStats(stats);

            Assert.AreEqual(6, stats.StatsCount);
            Assert.AreEqual(3, stats.SimpleStatsCount);
            Assert.AreEqual(3, stats.MinMaxStatsCount);

            stats.TryGetSimpleStat(FakeStatType.Speed, out var speedVal);
            Assert.AreEqual(10f, speedVal.Value);
        }

        [Test]
        public void _14_GetMultipleValues_ValuesAreCorrect()
        {
            var stats = new StatsController<FakeStatType>();
            Add3MinMaxStats(stats);
            Add3SimpleStats(stats);

            stats.TryGetSimpleStat(FakeStatType.Speed, out var speedVal);
            Assert.AreEqual(10f, speedVal.Value);

            stats.TryGetSimpleStat(FakeStatType.PhysicalDamage, out var pDamageVal);
            Assert.AreEqual(20f, pDamageVal.Value);

            stats.TryGetValue(FakeStatType.MagicalDamage, out var mDamageVal);
            Assert.AreEqual(30f, mDamageVal);

            stats.TryGetMinMaxStat(FakeStatType.Health, out var healthVal);
            Assert.AreEqual(40f, healthVal.Value);

            stats.TryGetMinMaxStat(FakeStatType.Mana, out var manaVal);
            Assert.AreEqual(50f, manaVal.Value);

            stats.TryGetValue(FakeStatType.PhysicalArmor, out var pArmorVal);
            Assert.AreEqual(60f, pArmorVal);
        }

        [Test]
        public void _15_TryRemoveSimpleStat_CanRemove()
        {
            var stats = new StatsController<FakeStatType>();
            Add3SimpleStats(stats);

            var result = stats.TryRemoveSimpleStat(FakeStatType.Speed);
            Assert.IsTrue(result);
            Assert.IsFalse(stats.TryGetValue(FakeStatType.Speed, out var speedVal));
            Assert.AreEqual(2, stats.StatsCount);
            Assert.AreEqual(2, stats.SimpleStatsCount);
        }

        [Test]
        public void _16_TryRemoveMinMaxStat_CanRemove()
        {
            var stats = new StatsController<FakeStatType>();
            Add3MinMaxStats(stats);

            var result = stats.TryRemoveMinMaxStat(FakeStatType.Health);
            Assert.IsTrue(result);
            Assert.IsFalse(stats.TryGetValue(FakeStatType.Health, out var healthVal));
            Assert.AreEqual(2, stats.StatsCount);
            Assert.AreEqual(2, stats.MinMaxStatsCount);
        }

        [Test]
        public void _17_TryRemoveStat_CanRemove()
        {
            var stats = new StatsController<FakeStatType>();
            Add3SimpleStats(stats);
            Add3MinMaxStats(stats);
            bool result = false;
            float val = 0f;

            result = stats.TryRemoveStat(FakeStatType.Health);
            Assert.IsTrue(result);
            Assert.IsFalse(stats.TryGetValue(FakeStatType.Health, out val));

            result = stats.TryRemoveStat(FakeStatType.Speed);
            Assert.IsTrue(result);
            Assert.IsFalse(stats.TryGetValue(FakeStatType.Speed, out val));

            Assert.AreEqual(4, stats.StatsCount);
            Assert.AreEqual(2, stats.SimpleStatsCount);
            Assert.AreEqual(2, stats.MinMaxStatsCount);
        }

        [Test]
        public void _18_ClearStats_CanClearAllStats()
        {
            var stats = new StatsController<FakeStatType>();
            Add3SimpleStats(stats);
            Add3MinMaxStats(stats);

            stats.ClearStats();

            Assert.AreEqual(0, stats.StatsCount);
            Assert.AreEqual(0, stats.SimpleStatsCount);
            Assert.AreEqual(0, stats.MinMaxStatsCount);
        }

        [Test]
        public void _19_TryGetIStat_CanGet()
        {
            var stats = new StatsController<FakeStatType>();
            Add3SimpleStats(stats);
            Add3MinMaxStats(stats);
            bool result = false;

            result = stats.TryGetIStat(FakeStatType.Health, out var healthStat);
            Assert.IsTrue(result);
            Assert.AreEqual(40f, healthStat.Value);

            result = stats.TryGetIStat(FakeStatType.PhysicalDamage, out var damageStat);
            Assert.IsTrue(result);
            Assert.AreEqual(20f, damageStat.Value);
        }


        private void Add3SimpleStats(StatsController<FakeStatType> stats)
        {
            stats.TryAddSimpleStat(FakeStatType.Speed, 10f);
            stats.TryAddSimpleStat(FakeStatType.PhysicalDamage, 20f);
            stats.TryAddSimpleStat(FakeStatType.MagicalDamage, 30f);
        }

        private void Add3MinMaxStats(StatsController<FakeStatType> stats)
        {
            stats.TryAddMinMaxStat(FakeStatType.Health, 40f);
            stats.TryAddMinMaxStat(FakeStatType.Mana, 50f);
            stats.TryAddMinMaxStat(FakeStatType.PhysicalArmor, 60f);
        }
    }
}