using NUnit.Framework;
using NUAssert = NUnit.Framework.Assert;
using Assert = UnityEngine.Assertions.Assert;

namespace GameSavvy.OUSL.StatSystem.Tests.EditMode
{
    public class _01_SimpleStatTests
    {
        [Test]
        public void _01_Exists_Yes()
        {
            var stats = new SimpleStat();
            NUAssert.NotNull(stats);
        }

        [Test]
        public void _02_IsStat_Yes()
        {
            var stats = new SimpleStat();
            NUAssert.IsInstanceOf<SimpleStat>(stats);
            NUAssert.IsInstanceOf<IStat>(stats);
        }

        [Test]
        public void _03_NewBaseValue_Is100()
        {
            var stats = new SimpleStat();
            Assert.AreApproximatelyEqual(100f, stats.BaseValue);
        }

        [Test]
        public void _04_NewPlusMod_Is0()
        {
            var stats = new SimpleStat();
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
        }

        [Test]
        public void _05_NewMultMod_Is1()
        {
            var stats = new SimpleStat();
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
        }

        [Test]
        public void _06_NewValue_Is100()
        {
            var stats = new SimpleStat();
            Assert.AreApproximatelyEqual(100f, stats.Value);
        }

        [TestCase(0f, 123f, 456f, 0f)]
        [TestCase(50f, 50f, 0f, 1f)]
        [TestCase(100f, 50f, 50f, 1f)]
        [TestCase(200f, 50f, 50f, 2f)]
        [TestCase(100f, 100f, 0f, 1f)]
        [TestCase(100f, 100f, -50f, 2f)]
        [TestCase(-100f, 100f, -50f, -2f)]
        public void _07_New_Cases(float expected, float baseValue, float plusMod, float multMod)
        {
            var stats = new SimpleStat(baseValue, plusMod, multMod);
            Assert.AreApproximatelyEqual(baseValue, stats.BaseValue);
            Assert.AreApproximatelyEqual(plusMod, stats.PlusMod);
            Assert.AreApproximatelyEqual(multMod, stats.MultMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(0f)]
        [TestCase(100f)]
        [TestCase(200f)]
        [TestCase(-123.456f)]
        public void _08_SetBaseValue_Cases(float baseValue)
        {
            var stats = new SimpleStat();
            stats.SetBaseValue(baseValue);
            Assert.AreApproximatelyEqual(baseValue, stats.BaseValue);
            Assert.AreApproximatelyEqual(baseValue, stats.Value);
        }

        [TestCase(100f, 0f)]
        [TestCase(150f, 50f)]
        [TestCase(50f, -50f)]
        [TestCase(-400f, -500f)]
        public void _09_SetPlusMod_Cases(float expected, float plusMod)
        {
            var stats = new SimpleStat();
            stats.SetPlusMod(plusMod);
            Assert.AreApproximatelyEqual(plusMod, stats.PlusMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(0f, 0f)]
        [TestCase(100f, 1f)]
        [TestCase(150f, 1.5f)]
        [TestCase(-200f, -2f)]
        public void _10_SetMultMod_Cases(float expected, float multMod)
        {
            var stats = new SimpleStat();
            stats.SetMultMod(multMod);
            Assert.AreApproximatelyEqual(multMod, stats.MultMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(100f, 0f, 0f)]
        [TestCase(130f, 10f, 20f)]
        [TestCase(-80f, -200f, 20f)]
        public void _11_AddPlusMod_Cases(float expected, float plusMod1, float plusMod2)
        {
            var stats = new SimpleStat();
            stats.AddPlusMod(plusMod1);
            stats.AddPlusMod(plusMod2);
            Assert.AreApproximatelyEqual(plusMod1 + plusMod2, stats.PlusMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(100f, 0f, 0f)]
        [TestCase(200f, 0f, 1f)]
        [TestCase(200f, 1f, 0f)]
        [TestCase(300f, 1f, 1f)]
        [TestCase(0f, -1f, 0f)]
        [TestCase(0f, 0f, -1f)]
        [TestCase(400f, 1f, 2f)]
        [TestCase(100f * (1f + 2f + 3f), 2f, 3f)]
        [TestCase(100f * (1f - 2f + 3f), -2f, 3f)]
        [TestCase(100f * 1.3f, 0.1f, 0.2f)]
        [TestCase(100f * 0.7f, -0.1f, -0.2f)]
        public void _12_AddMultMod_Cases(float expected, float multMod1, float multMod2)
        {
            var stats = new SimpleStat();
            stats.AddMultMod(multMod1);
            stats.AddMultMod(multMod2);
            Assert.AreApproximatelyEqual(1 + multMod1 + multMod2, stats.MultMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(100f, 0f, 1f)]
        [TestCase(100f, 2f, 3f)]
        [TestCase(50f, -2f, -3f)]
        public void _13_ResetMods_Cases(float expected, float plusMod, float multMod)
        {
            var stats = new SimpleStat(expected, plusMod, multMod);
            stats.ResetMods();
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [TestCase(100f, 0f, 1f, 2f)]
        [TestCase(100f, 2f, 3f, 4f)]
        [TestCase(50f, -2f, -3f, -7f)]
        [TestCase(-50f, 22f, 33f, 9f)]
        public void _14_ResetTo_Cases(float expected, float newBase, float plusMod, float multMod)
        {
            var stats = new SimpleStat(newBase, plusMod, multMod);
            stats.ResetTo(expected);
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
            Assert.AreApproximatelyEqual(expected, stats.Value);
        }

        [Test]
        public void _15_OnChange_SetBaseValue_PerformsCallback()
        {
            var stats = new SimpleStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.SetBaseValue(90f);
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _16_OnChange_AddPlusMod_PerformsCallback()
        {
            var stats = new SimpleStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.AddPlusMod(-10f);
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _17_OnChange_AddMultMod_PerformsCallback()
        {
            var stats = new SimpleStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.AddMultMod(-0.1f);
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _18_OnChange_SetPlusMod_PerformsCallback()
        {
            var stats = new SimpleStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.SetPlusMod(-10f);
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _19_OnChange_SetMultMod_PerformsCallback()
        {
            var stats = new SimpleStat();
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.SetMultMod(0.9f);
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(90f, newVal);
            Assert.AreApproximatelyEqual(-10f, delta);
        }

        [Test]
        public void _20_OnChange_ResetMods_PerformsCallback()
        {
            var stats = new SimpleStat(100f, -10f, 0.9f);
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.ResetMods();
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(100f, newVal);
            Assert.AreApproximatelyEqual(100f - 81f, delta);
        }

        [Test]
        public void _21_OnChange_ResetMods_PerformsCallback()
        {
            var stats = new SimpleStat(100f, -10f, 0.9f);
            var called = false;
            float newVal = 0f;
            float delta = 0f;

            stats.OnChange += (v, d) => (called, newVal, delta) = (true, v, d);
            stats.ResetTo(150f);
            Assert.IsTrue(called);
            Assert.AreApproximatelyEqual(150f, stats.Value);
            Assert.AreApproximatelyEqual(0f, stats.PlusMod);
            Assert.AreApproximatelyEqual(1f, stats.MultMod);
            Assert.AreApproximatelyEqual(150f, newVal);
            Assert.AreApproximatelyEqual(150f - 81f, delta);
        }
    }
}