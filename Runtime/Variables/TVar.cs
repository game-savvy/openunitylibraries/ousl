﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [InlineProperty]
    [BoxGroup("_TVar_Raw,Event,Asset", false)]
    [Serializable]
    public class TVar<T>
    {
        private enum UseVarType : byte
        {
            RawValue,
            ValueEvent,
            ValueAsset
        }

        public static implicit operator T(TVar<T> v) => v.Value;

        [VerticalGroup("Reference")]
        [HideLabel]
        [EnumToggleButtons]
        [GUIColor(0.45f, 0.65f, 0.65f, 1f)]
        [SerializeField]
        private UseVarType _varType = UseVarType.RawValue;

        [ShowIf("IsRawValue", Animate = false)]
        [VerticalGroup("Reference")]
        [LabelWidth(50), LabelText("Value")]
        [PropertySpace(SpaceBefore = 0, SpaceAfter = 4)]
        [SerializeField]
        private T _rawValue;

        [ShowIf("IsValueEvent", Animate = false)]
        [VerticalGroup("Reference")]
        [HideLabel]
        [PropertySpace(SpaceBefore = 0, SpaceAfter = 4)]
        [SerializeField]
        private TValue<T> _eventValue;

        [ShowIf("IsValueAsset", Animate = false)]
        [VerticalGroup("Reference")]
        [HideLabel]
        [InlineEditor(InlineEditorObjectFieldModes.Boxed, Expanded = true)]
        [AssetsOnly]
        [SerializeField]
        private TAsset<T> _varAsset;


        public T Value
        {
            get => _varType switch {UseVarType.ValueEvent => _eventValue.Value, UseVarType.ValueAsset => _varAsset.Value, _ => _rawValue};

            set
            {
                switch (_varType)
                {
                    case UseVarType.RawValue:
                        _rawValue = value;
                        break;

                    case UseVarType.ValueEvent:
                        _eventValue.Value = value;
                        break;

                    case UseVarType.ValueAsset:
                        _varAsset.Value = value;
                        break;

                    default: break;
                }
            }
        }

        public event Action<T> OnChange
        {
            add
            {
                switch (_varType)
                {
                    case UseVarType.ValueEvent:
                        _eventValue.OnChange += value;
                        break;

                    case UseVarType.ValueAsset:
                        _varAsset.Observers += value;
                        break;

                    default: break;
                }
            }

            remove
            {
                switch (_varType)
                {
                    case UseVarType.ValueEvent:
                        _eventValue.OnChange -= value;
                        break;

                    case UseVarType.ValueAsset:
                        _varAsset.Observers -= value;
                        break;

                    default: break;
                }
            }
        }

        private bool IsRawValue() => _varType == UseVarType.RawValue;
        private bool IsValueEvent() => _varType == UseVarType.ValueEvent;
        private bool IsValueAsset() => _varType == UseVarType.ValueAsset;
    }
}