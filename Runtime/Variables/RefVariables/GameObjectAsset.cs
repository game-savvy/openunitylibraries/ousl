using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/GameObject")]
    public class GameObjectAsset : TAsset<GameObject> { }
}
