using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Vector3")]
    public class Vector3Asset : TAsset<Vector3> { }
}
