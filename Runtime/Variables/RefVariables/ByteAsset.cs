using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Byte")]
    public class ByteAsset : TAsset<byte> { }
}
