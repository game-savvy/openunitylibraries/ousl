using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Transform")]
    public class TransformAsset : TAsset<Transform> { }
}
