using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Rigidbody2D")]
    public class Rigidbody2DAsset : TAsset<Rigidbody2D> { }
}
