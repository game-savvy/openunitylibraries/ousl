using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/UShort")]
    public class UShortAsset : TAsset<ushort> { }
}
