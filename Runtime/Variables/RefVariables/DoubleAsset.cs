using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Double")]
    public class DoubleAsset : TAsset<double> { }
}
