using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/SByte")]
    public class SByteAsset : TAsset<sbyte> { }
}
