﻿using System;
using Sirenix.OdinInspector;

namespace GameSavvy.OpenUnityTools.Variables
{
    [Serializable]
    // [BoxGroup("_GameEvent_", false)]
    [InlineButton("@$value.NotifyObservers()", "Notify Observers!")]
    public class GameEvent
    {
        public event Action Observers;

        public void NotifyObservers()
        {
            if (Observers != null)
            {
                Observers();
            }
        }
    }
}
