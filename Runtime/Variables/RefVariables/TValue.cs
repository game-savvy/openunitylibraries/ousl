﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [InlineProperty]
    [Serializable]
    public class TValue<T>
    {
        public static implicit operator T(TValue<T> v) => v.Value;

        public event Action<T> OnChange;

        [HideInInspector]
        [SerializeField]
        protected T _value;

        [InlineButton("NotifyObservers", " ! ")]
        [PropertySpace(SpaceBefore = 0, SpaceAfter = 4)]
        [LabelWidth(50)]
        [ShowInInspector]
        public virtual T Value
        {
            get => _value;
            set
            {
                _value = value;
                NotifyObservers();
            }
        }

        public void NotifyObservers() => OnChange?.Invoke(_value);
    }
}