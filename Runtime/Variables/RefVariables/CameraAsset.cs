using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Camera")]
    public class CameraAsset : TAsset<Camera> { }
}
