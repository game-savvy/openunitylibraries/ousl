﻿using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace GameSavvy.OUSL.Variables
{
    [Serializable]
    public abstract class TAsset<T> : ScriptableObject
    {
        public static implicit operator T(TAsset<T> v) => v.Value;

        public event Action<T> Observers;

        [InlineButton("NotifyObservers", " ! ")]
        [PropertySpace(SpaceBefore = 0, SpaceAfter = 4)]
        [LabelWidth(100)]
        [OnValueChanged("NotifyObservers")]
        [SerializeField]
        protected T _value;

        public T Value
        {
            get => _value;
            set
            {
                _value = value;
                NotifyObservers();
            }
        }

        public void NotifyObservers()
        {
            Observers?.Invoke(_value);
        }
    }
}