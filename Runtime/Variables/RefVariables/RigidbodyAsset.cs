using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Rigidbody")]
    public class RigidbodyAsset : TAsset<Rigidbody> { }
}
