using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/AudioClip")]
    public class AudioClipAsset : TAsset<AudioClip> { }
}
