using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Quaternion")]
    public class QuaternionAsset : TAsset<Quaternion> { }
}
