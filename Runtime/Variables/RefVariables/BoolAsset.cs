using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Bool")]
    public class BoolAsset : TAsset<bool> { }
}
