﻿using System;
using GameSavvy.OpenUnityTools.Variables;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/GameEvent")]
    public class GameEventObj : ScriptableObject
    {
        public event Action Observers;

        [Button]
        public void NotifyObservers()
        {
            if (Observers != null)
            {
                Observers();
            }
        }
    }
}
