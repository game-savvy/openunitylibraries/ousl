using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/ULong")]
    public class ULongAsset : TAsset<ulong> { }
}
