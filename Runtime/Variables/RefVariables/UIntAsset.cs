using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/UInt")]
    public class UIntAsset : TAsset<uint> { }
}
