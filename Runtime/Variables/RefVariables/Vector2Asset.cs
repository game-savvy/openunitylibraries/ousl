using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Vector2")]
    public class Vector2Asset : TAsset<Vector2> { }
}
