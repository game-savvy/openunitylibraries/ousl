﻿using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
	public class OnRigidbody2DObjChanged : OnTObjChanged<Rigidbody2D, Rigidbody2DAsset, UEvent_Rigidbody2D> { }
}
