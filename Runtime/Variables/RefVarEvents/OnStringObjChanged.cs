using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnStringObjChanged : OnTObjChanged<string, StringAsset, UEvent_String> { }
}
