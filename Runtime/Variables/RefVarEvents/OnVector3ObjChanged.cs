using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnVector3ObjChanged : OnTObjChanged<Vector3, Vector3Asset, UEvent_Vector3> { }
}
