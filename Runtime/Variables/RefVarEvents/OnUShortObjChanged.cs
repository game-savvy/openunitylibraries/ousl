using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnUShortObjChanged : OnTObjChanged<ushort, UShortAsset, UEvent_UShort> { }
}
