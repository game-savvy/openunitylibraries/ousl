using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnUIntObjChanged : OnTObjChanged<uint, UIntAsset, UEvent_UInt> { }
}
