using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnIntObjChanged : OnTObjChanged<int, IntAsset, UEvent_Int> { }
}
