﻿using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnFloatObjChanged : OnTObjChanged<float, FloatAsset, UEvent_Float> { }
}
