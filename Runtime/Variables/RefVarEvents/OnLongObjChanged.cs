using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
	public class OnLongObjChanged : OnTObjChanged<long, LongAsset, UEvent_Long> { }
}
