using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnULongObjChanged : OnTObjChanged<ulong, ULongAsset, UEvent_ULong> { }
}
