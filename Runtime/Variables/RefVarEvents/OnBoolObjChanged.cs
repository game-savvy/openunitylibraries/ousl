﻿using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnBoolObjChanged : OnTObjChanged<bool, BoolAsset, UEvent_Bool> { }
}
