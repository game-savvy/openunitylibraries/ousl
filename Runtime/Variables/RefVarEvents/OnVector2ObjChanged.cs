using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnVector2ObjChanged : OnTObjChanged<Vector2, Vector2Asset, UEvent_Vector2> { }
}
