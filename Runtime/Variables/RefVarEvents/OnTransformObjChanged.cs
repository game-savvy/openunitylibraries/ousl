﻿using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnTransformObjChanged : OnTObjChanged<Transform, TransformAsset, UEvent_Transform> { }
}
