using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnQuaternionObjChanged : OnTObjChanged<Quaternion, QuaternionAsset, UEvent_Quaternion> { }
}
