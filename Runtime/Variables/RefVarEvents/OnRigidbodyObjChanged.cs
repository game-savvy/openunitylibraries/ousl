﻿using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnRigidbodyObjChanged : OnTObjChanged<Rigidbody, RigidbodyAsset, UEvent_Rigidbody> { }
}
