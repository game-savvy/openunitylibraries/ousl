﻿using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnByteObjChanged : OnTObjChanged<byte, ByteAsset, UEvent_Byte> { }
}
