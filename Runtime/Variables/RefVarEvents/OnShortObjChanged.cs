using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnShortObjChanged : OnTObjChanged<short, ShortAsset, UEvent_Short> { }
}
