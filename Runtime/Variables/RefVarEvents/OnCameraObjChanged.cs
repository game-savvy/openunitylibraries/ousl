﻿using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnCameraObjChanged : OnTObjChanged<Camera, CameraAsset, UEvent_Camera> { }
}
