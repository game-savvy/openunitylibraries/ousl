﻿using GameSavvy.OpenUnityTools.Variables;
using UnityEngine;

namespace GameSavvy.OUSL.Variables
{
    public class OnGameObjectObjChanged : OnTObjChanged<GameObject, GameObjectAsset, UEvent_GameObject> { }
}
