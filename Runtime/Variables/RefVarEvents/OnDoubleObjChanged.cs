﻿using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnDoubleObjChanged : OnTObjChanged<double, DoubleAsset, UEvent_Double> { }
}
