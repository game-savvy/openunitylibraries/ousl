﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.OUSL.Variables
{

    public abstract class OnTObjChanged<T, TObject, UEvent> : MonoBehaviour where TObject : TAsset<T> where UEvent : UnityEvent<T>
    {
        [SerializeField]
        [Required]
        [AssetsOnly]
        protected TObject _TObj;

        public UEvent Actions;

        [SerializeField]
        protected bool _NotifyOnEnable = false;

        private bool CanUse => (_TObj != null && _TObj.Value != null);

        protected void OnEnable()
        {
            if (_TObj == null)
            {
                return;
            }

            _TObj.Observers += TriggerActions;
            if (_NotifyOnEnable)
            {
                TriggerActionsButton();
            }
        }

        protected void OnDisable()
        {
            if (_TObj == null)
            {
                return;
            }

            _TObj.Observers -= TriggerActions;
        }

        private void TriggerActions(T val)
        {
            Actions.Invoke(val);
        }

        [Button]
        public void TriggerActionsButton()
        {
            if (CanUse)
            {
                TriggerActions(_TObj.Value);
            }
        }

    }
}
