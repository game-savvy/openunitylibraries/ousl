using GameSavvy.OpenUnityTools.Variables;

namespace GameSavvy.OUSL.Variables
{
    public class OnSByteObjChanged : OnTObjChanged<sbyte, SByteAsset, UEvent_SByte> { }
}
