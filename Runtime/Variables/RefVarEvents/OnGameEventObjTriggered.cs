﻿using GameSavvy.OUSL.Variables;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;


public class OnGameEventObjTriggered : MonoBehaviour
{

#pragma warning disable 649
    [SerializeField]
    [Required]
    [AssetsOnly]
    private GameEventObj _GameEvent;

    [SerializeField]
    private UnityEvent _Actions;
#pragma warning restore 649

    private void OnEnable()
    {
        _GameEvent.Observers += TriggerActions;
    }

    private void OnDisable()
    {
        _GameEvent.Observers -= TriggerActions;
    }

    [Button]
    public void TriggerActions()
    {
        _Actions?.Invoke();
    }

}
