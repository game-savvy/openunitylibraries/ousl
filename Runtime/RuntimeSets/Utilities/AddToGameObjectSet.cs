﻿using GameSavvy.OUSL.RuntimeSets;
using UnityEngine;

public class AddToGameObjectSet : AddToTSet<GameObject>
{
    protected override void Awake()
    {
        _toAdd = gameObject;
        if (_onlyIfEnabed == false)
        {
            _set.AddItem(_toAdd);
        }
    }
}