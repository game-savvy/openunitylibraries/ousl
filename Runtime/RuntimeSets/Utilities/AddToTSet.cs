﻿using UnityEngine;

namespace GameSavvy.OUSL.RuntimeSets
{
    /// <summary>
    ///  Base class for adding a component to a RuntimeSet
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AddToTSet<T> : MonoBehaviour
    {
        [SerializeField, Tooltip("False = Awake/OnDestroy;\nTrue = OnEnable/OnDisable")]
        protected bool _onlyIfEnabed = false;

        [SerializeField]
        protected RuntimeSet<T> _set;

        protected T _toAdd;

        protected virtual void Awake()
        {
            _toAdd = GetComponent<T>();
            if (_onlyIfEnabed == false && _toAdd != null)
            {
                _set?.AddItem(_toAdd);
            }
        }

        protected void OnEnable()
        {
            if (_onlyIfEnabed && _toAdd != null)
            {
                _set?.AddItem(_toAdd);
            }
        }

        protected void OnDisable()
        {
            if (_onlyIfEnabed && _toAdd != null)
            {
                _set?.RemoveItem(_toAdd);
            }
        }

        protected void OnDestroy()
        {
            if (_onlyIfEnabed == false && _toAdd != null)
            {
                _set?.RemoveItem(_toAdd);
            }
        }
    }
}