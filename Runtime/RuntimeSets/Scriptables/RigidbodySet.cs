﻿using UnityEngine;

namespace GameSavvy.OUSL.RuntimeSets
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/RigidbodySet")]
    public class RigidbodySet : RuntimeSet<Rigidbody>
    {
    }
}