﻿using UnityEngine;

namespace GameSavvy.OUSL.RuntimeSets
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/Rigidbody2DSet")]
    public class Rigidbody2DSet : RuntimeSet<Rigidbody2D>
    {
    }
}