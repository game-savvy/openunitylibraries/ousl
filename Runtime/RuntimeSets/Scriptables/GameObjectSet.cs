﻿using UnityEngine;

namespace GameSavvy.OUSL.RuntimeSets
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/GameObjectSet")]
    public class GameObjectSet : RuntimeSet<GameObject>
    {
    }
}