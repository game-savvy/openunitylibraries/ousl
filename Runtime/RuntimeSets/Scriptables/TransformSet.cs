﻿using UnityEngine;

namespace GameSavvy.OUSL.RuntimeSets
{
	[System.Serializable]
	[CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/TransformSet")]
	public class TransformSet : RuntimeSet<Transform> { }
}
