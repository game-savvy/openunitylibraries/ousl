﻿using UnityEngine;
using UnityEngine.AI;

namespace GameSavvy.OUSL.RuntimeSets
{
    [System.Serializable]
    [CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/NavMeshAgentSet")]
    public class NavMeshAgentSet : RuntimeSet<NavMeshAgent>
    {
    }
}