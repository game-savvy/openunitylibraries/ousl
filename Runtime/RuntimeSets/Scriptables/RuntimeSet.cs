﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameSavvy.OUSL.RuntimeSets
{
    [Serializable]
    public abstract class RuntimeSet<T> : ScriptableObject
    {
        public List<T> Items = new();

        public event Action<T> OnItemAdded;
        public event Action<T> OnItemRemoved;

        public virtual void AddItem(T item)
        {
            if (item == null || Items.Contains(item)) return;
            Items.Add(item);
            OnItemAdded?.Invoke(item);
        }

        public virtual void RemoveItem(T item)
        {
            if (item == null || Items.Contains(item) == false) return;
            Items.Remove(item);
            OnItemRemoved?.Invoke(item);
        }

        public virtual bool ContainsItem(T item) => item != null && Items.Contains(item);


        [Button("Remove Null Items")]
        [ExecuteInEditMode]
        public virtual void RemoveNullItems()
        {
            int removedCount = 0;
            for (int i = Items.Count - 1; i >= 0; i--)
            {
                if (Items[i] == null || Items[i].ToString() == "null")
                {
                    ++removedCount;
                    Items.RemoveAt(i);
                }
            }

            Debug.Log($"Removed {removedCount} NULL Items", this);
        }

        [Button("Remove Repeated Items")]
        [ExecuteInEditMode]
        public virtual void RemoveRepeatedItems()
        {
            if (Items == null) return;

            int itemCount = Items.Count;

            var hs = new HashSet<T>(Items);
            Items = hs.ToList();

            itemCount -= Items.Count;
            Debug.Log($"Removed {itemCount} REPEATED Items", this);
        }

        [Button]
        [ExecuteInEditMode]
        public void ClearList()
        {
            RemoveNullItems();
            RemoveRepeatedItems();
        }
    }
}