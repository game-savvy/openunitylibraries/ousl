﻿namespace GameSavvy.OUSL.BuffSystem
{
    public interface IBuffable<TCtx>
    {
        bool ApplyBuff(ABuffConfig<TCtx> buff, TCtx applier);
        bool ApplyBuff(ABuff<TCtx> buff);

        bool CleanseBuff(ABuffConfig<TCtx> buff, TCtx applier);
        bool CleanseBuff(ABuff<TCtx> buff);
    }
}