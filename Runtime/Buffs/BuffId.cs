using System;
using Sirenix.OdinInspector;

namespace GameSavvy.OUSL.BuffSystem
{
    [HideReferenceObjectPicker]
    [Serializable]
    public record BuffId<TCtx>(ABuffConfig<TCtx> Config, TCtx Receiver, TCtx Applier)
    {
        [HideLabel]
        [ShowInInspector]
        public string Name => Config.Name;

        public ABuffConfig<TCtx> Config { get; } = Config;
        public TCtx Receiver { get; } = Receiver;
        public TCtx Applier { get; } = Applier;
    }
}