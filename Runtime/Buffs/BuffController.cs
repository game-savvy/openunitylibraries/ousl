﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace GameSavvy.OUSL.BuffSystem
{
    [Serializable]
    public class BuffController<TCtx> : IBuffable<TCtx>
    {
        public readonly TCtx Receiver;
        public readonly HashSet<BuffId<TCtx>> BuffsToRemove;

        [ReadOnly]
        [HideReferenceObjectPicker]
        [ShowInInspector]
        public readonly Dictionary<BuffId<TCtx>, ABuff<TCtx>> ActiveBuffs;

        public int BuffCount => ActiveBuffs?.Count ?? 0;

        public BuffController(TCtx receiver)
        {
            Receiver = receiver;
            ActiveBuffs = new(4);
            BuffsToRemove = new();
        }

        public bool ApplyBuff(ABuffConfig<TCtx> buffConfig, TCtx applier)
        {
            if (buffConfig == null) return false;
            BuffId<TCtx> buffId = new(buffConfig, Receiver, applier);
            bool hasValue = ActiveBuffs.TryGetValue(buffId, out var buff);
            if (hasValue == false)
            {
                buff = buffConfig.GenerateBuff(Receiver, applier);
                ActiveBuffs.Add(buffId, buff);
            }

            buff.SetActive(true);
            return true;
        }

        public bool ApplyBuff(ABuff<TCtx> buff)
        {
            if (buff == null) return false;
            var buffId = buff.Id;
            bool hasValue = ActiveBuffs.TryGetValue(buffId, out var existingBuff);
            if (hasValue == false)
            {
                ActiveBuffs.Add(buffId, buff);
            }

            buff.SetActive(true);
            return true;
        }

        public bool CleanseBuff(ABuffConfig<TCtx> buffConfig, TCtx applier)
        {
            if (buffConfig == null) return false;
            BuffId<TCtx> buffId = new(buffConfig, Receiver, applier);
            bool hasValue = ActiveBuffs.TryGetValue(buffId, out var existingBuff);
            if (hasValue == false) return false;
            existingBuff.Cleanse();

            return true;
        }

        public bool CleanseBuff(ABuff<TCtx> buff)
        {
            if (buff == null) return false;
            BuffId<TCtx> buffId = buff.Id;
            bool hasValue = ActiveBuffs.TryGetValue(buffId, out var existingBuff);
            if (hasValue == false) return false;
            existingBuff.Cleanse();

            return true;
        }


        public void Update(float deltaTime)
        {
            RemoveAllBuffsToRemove();

            // Handle Update Buffs
            foreach (var buff in ActiveBuffs.Values)
            {
                if (buff.IsActive == false)
                {
                    BuffsToRemove.Add(buff.Id);
                    continue;
                }
                buff.Update(deltaTime);
            }
        }

        public void ClearAllActiveBuffs()
        {
            foreach (var buff in ActiveBuffs.Values)
            {
                buff.SetActive(false);
            }

            ActiveBuffs.Clear();
            BuffsToRemove.Clear();
        }

        public void CleanseAllBuffs()
        {
            foreach (var buff in ActiveBuffs.Values)
            {
                buff.Cleanse();
            }

            RemoveAllBuffsToRemove();
        }

        private void RemoveAllBuffsToRemove()
        {
            foreach (var buffId in BuffsToRemove)
            {
                ActiveBuffs.Remove(buffId);
            }

            BuffsToRemove.Clear();
        }
    }
}