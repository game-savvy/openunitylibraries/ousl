﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.BuffSystem
{
    [HideReferenceObjectPicker]
    [Serializable]
    public abstract class ABuff<TCtx>
    {
        [HideInInspector]
        public readonly BuffId<TCtx> Id;

        public TCtx Receiver => Id.Receiver;
        public TCtx Applier => Id.Applier;

        protected bool _isActive = false;
        public bool IsActive => _isActive;

        [HideLabel]
        [ShowIf("ShowDescription")]
        [PropertyOrder(-1)]
        [ShowInInspector]
        public virtual string Description => Id.Config.Description;

        [ShowIf("IsStackable")]
        [ShowInInspector]
        public int StackCount => _stackCount;
        protected int _stackCount;

        protected int _maxStacks;
        public int MaxStacks => _maxStacks;

        [ShowIf("IsTimed")]
        [ShowInInspector]
        public string PercentString => Percent.ToString("P");
        public float Percent => TickTimer?.Percent ?? 0f;

        [ShowIf("IsTimed")]
        [ShowInInspector]
        public float TimeRemaining => IsTimed ? (1f - Percent) * TickTimer.Duration : 0f;

        public bool IsTimed => TickTimer != null && TickTimer.Duration > 0f;
        public bool IsTickable => TickTimer?.IsTickable ?? false;
        public bool IsStackable => _maxStacks > 1;
        protected virtual bool ShowDescription => string.IsNullOrWhiteSpace(Id.Config.Description.Trim()) == false;

        [HideInInspector]
        public readonly TickableTimer TickTimer;


        protected ABuff(ABuffConfig<TCtx> config, TCtx receiver, TCtx applier)
        {
            _maxStacks = config.MaxStacks;
            _stackCount = 0;

            Id = new(config, receiver, applier);

            if (config.IsTimed)
            {
                TickTimer = new(config.Duration, config.TargetTickCount, OnTimerTick, ExpireTimer);
                OnTimerStart();
            }
        }

        /// <summary>
        /// Deals with OnApply, OnReApply, OnRemove, and OnStackAdded
        /// </summary>
        /// <param name="newValue"></param>
        public virtual void SetActive(bool newValue)
        {
            if (_isActive == newValue)
            {
                ReApply();
                IncrementStacks();
                return;
            }

            _isActive = newValue;
            if (_isActive)
            {
                IncrementStacks();
                OnApply();
            }
            else
            {
                OnRemove();
            }
        }

        public virtual void Update(float deltaTime)
        {
            TickTimer?.Update(deltaTime);
            OnUpdate(deltaTime);
        }

        public virtual void Cleanse()
        {
            SetActive(false);
            OnCleanse();
        }


        protected virtual void IncrementStacks()
        {
            if (IsStackable == false) return;
            if (_stackCount > _maxStacks) return;

            _stackCount++;
            OnStackAdded(_stackCount);
            if (_stackCount == _maxStacks)
            {
                OnMaxStacksReached();
            }
        }

        protected virtual void ReApply()
        {
            TickTimer?.Restart();
            OnReApply();
        }

        protected virtual void ExpireTimer()
        {
            SetActive(false);
            OnTimerExpired();
        }


        protected virtual void OnApply() { }
        protected virtual void OnReApply() { }
        protected virtual void OnTimerStart() { }
        protected virtual void OnTimerExpired() { }
        protected virtual void OnUpdate(float deltaTime) { }
        protected virtual void OnTimerTick() { }
        protected virtual void OnStackAdded(int newStackCount) { }
        protected virtual void OnMaxStacksReached() { }
        protected virtual void OnCleanse() { }
        protected virtual void OnRemove() { }
    }
}