using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.BuffSystem
{
    [HideReferenceObjectPicker]
    public class TickableTimer
    {
        private readonly Action _onExpire;
        private readonly Action _onTick;
        private readonly float _timeBetweenTicks;

        private float _elapsedTime;
        private float _elapsedTickTime;

        private bool _isRunning;
        public bool IsRunning => _isRunning;

        private int _curTickCount;
        public int CurrentTickCount => _curTickCount;

        private readonly int _targetTickCount;
        public int TargetTickCount => _targetTickCount;

        public readonly float Duration;
        public float Percent => _elapsedTime / Duration;
        public bool IsTickable => _targetTickCount > 1;

        public TickableTimer(float duration, int targetTickCount, Action onTick, Action onExpire)
        {
            Duration = duration;
            _targetTickCount = targetTickCount;
            _timeBetweenTicks = _targetTickCount > 1 ? duration / (targetTickCount - 1f) : float.MaxValue;

            _onTick = onTick;
            _onExpire = onExpire;

            Restart();
        }

        public void Restart()
        {
            _curTickCount = 0;
            _elapsedTime = 0f;
            _elapsedTickTime = -1f * (_timeBetweenTicks + Time.deltaTime);
            _isRunning = true;
        }

        public void Update(float deltaTime)
        {
            if (_isRunning == false) return;

            _elapsedTime += deltaTime;
            _elapsedTickTime += deltaTime;

            // for the first Tick, we want to tick immediately and reset the elapsed tick time
            if (_elapsedTickTime < 0f)
            {
                _elapsedTickTime = deltaTime;
                _curTickCount = 1;
                _onTick?.Invoke();
            }
            else if (_elapsedTickTime >= _timeBetweenTicks)
            {
                _curTickCount++;
                _onTick?.Invoke(); // This also performs the last tick
                _elapsedTickTime -= _timeBetweenTicks;
            }

            if (_elapsedTime >= Duration)
            {
                _onExpire?.Invoke();
                _elapsedTime = Duration;
                _isRunning = false;
            }
        }
    }
}