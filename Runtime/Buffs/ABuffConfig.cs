﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.BuffSystem
{
    public abstract class ABuffConfig<TCtx> : ScriptableObject
    {
        public enum BuffTypes
        {
            Passive,
            Buff,
            Debuff
        }

        [PropertyOrder(-3)]
        [BoxGroup("Buff")]
        [SerializeField]
        protected string _name;
        public string Name => _name;

        [PropertyOrder(-3)]
        [BoxGroup("Buff")]
        [SerializeField]
        protected string _description;
        public string Description => _description;

        [PropertyOrder(-3)]
        [BoxGroup("Buff")]
        [AssetsOnly]
        [SerializeField]
        protected Sprite _icon;
        public Sprite Icon => _icon;

        [PropertyOrder(-3)]
        [BoxGroup("Buff")]
        [SerializeField]
        protected BuffTypes _buffType;
        public BuffTypes BuffType => _buffType;

        [Header("Buff Settings")]
        [PropertyOrder(-2)]
        [BoxGroup("Buff")]
        [SerializeField]
        protected float _duration = 0f;
        public float Duration => _duration;

        [PropertyOrder(-2)]
        [BoxGroup("Buff")]
        [SerializeField]
        protected int _targetTickCount = 0;
        public int TargetTickCount => _targetTickCount;

        [PropertyOrder(-2)]
        [BoxGroup("Buff")]
        [SerializeField]
        protected int _maxStacks = 0;
        public int MaxStacks => _maxStacks;


        [PropertyOrder(-1)]
        [HorizontalGroup("Buff/Properties")]
        [LabelWidth(52)]
        [ShowInInspector]
        public bool IsTimed => _duration > 0f;

        [PropertyOrder(-1)]
        [HorizontalGroup("Buff/Properties")]
        [LabelWidth(65)]
        [ShowInInspector]
        public bool IsTickable => _targetTickCount > 1;

        [PropertyOrder(-1)]
        [HorizontalGroup("Buff/Properties")]
        [LabelWidth(73)]
        [ShowInInspector]
        public bool IsStackable => _maxStacks > 1;

        [PropertyOrder(-1)]
        [HorizontalGroup("Buff/Properties")]
        [LabelWidth(80)]
        [ShowInInspector]
        public float TickDuration => _targetTickCount < 2 ? 0f : _duration / (_targetTickCount - 1);


        public abstract ABuff<TCtx> GenerateBuff(TCtx receiver, TCtx applier);
    }
}