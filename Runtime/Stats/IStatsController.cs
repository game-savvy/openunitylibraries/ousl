using System;

namespace GameSavvy.OUSL.StatSystem
{
    public interface IStatsController<TEnum> where TEnum : Enum
    {
        int SimpleStatsCount { get; }
        int MinMaxStatsCount { get; }
        int StatsCount { get; }

        bool TryAddStat(TEnum key, SimpleStat stat);
        bool TryAddStat(TEnum key, MinMaxStat stat);
        bool TryAddSimpleStat(TEnum key, float value);
        bool TryAddMinMaxStat(TEnum key, float value, float min = 0f, float max = 100f);

        bool TryGetSimpleStat(TEnum key, out SimpleStat stat);
        bool TryGetMinMaxStat(TEnum key, out MinMaxStat stat);
        bool TryGetIStat(TEnum key, out IStat stat);
        bool TryGetValue(TEnum key, out float val);

        bool TryRemoveSimpleStat(TEnum key);
        bool TryRemoveMinMaxStat(TEnum key);
        bool TryRemoveStat(TEnum key);

        void ClearStats();
    }
}