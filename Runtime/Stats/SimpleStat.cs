using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.StatSystem
{
    [HideReferenceObjectPicker]
    [Serializable]
    public class SimpleStat : IStat
    {
        public event Action<float, float> OnChange;

        /// <summary>
        /// Gets the value of the stat after all modifiers have been applied.
        /// Sets the base value of the stat.
        /// </summary>
        [BoxGroup("__SIMPLE_STAT__", ShowLabel = false)]
        [LabelWidth(100)]
        [ShowInInspector]
        [PropertyOrder(-1)]
        public float Value
        {
            get => (BaseValue + PlusMod) * MultMod;
            set => SetBaseValue(value);
        }

        [FoldoutGroup("__SIMPLE_STAT__/Show")]
        [LabelWidth(100)]
        [PropertyOrder(0)]
        [SerializeField]
        private float _baseValue = 100f;
        public float BaseValue => _baseValue;

        [FoldoutGroup("__SIMPLE_STAT__/Show")]
        [LabelWidth(100)]
        [PropertyOrder(0)]
        [SerializeField]
        private float _plusMod = 0f;
        public float PlusMod => _plusMod;

        [FoldoutGroup("__SIMPLE_STAT__/Show")]
        [LabelWidth(100)]
        [PropertyOrder(0)]
        [SerializeField]
        private float _multMod = 1f;
        public float MultMod => _multMod;

        [FoldoutGroup("__SIMPLE_STAT__/Show")]
        [Button]
        public void NotifyObservers() => OnChange?.Invoke(Value, 0f);


        public SimpleStat(float baseValue = 100f, float plusMod = 0f, float multMod = 1f)
        {
            _baseValue = baseValue;
            _plusMod = plusMod;
            _multMod = multMod;
            OnChange = null;
        }

        public void SetBaseValue(float val)
        {
            var old = Value;
            _baseValue = val;
            OnChange?.Invoke(Value, Value - old);
        }

        public void SetPlusMod(float modVal)
        {
            var old = Value;
            _plusMod = modVal;
            OnChange?.Invoke(Value, Value - old);
        }

        public void SetMultMod(float modVal)
        {
            var old = Value;
            _multMod = modVal;
            OnChange?.Invoke(Value, Value - old);
        }

        public void AddPlusMod(float modVal)
        {
            var old = Value;
            _plusMod += modVal;
            OnChange?.Invoke(Value, Value - old);
        }

        public void AddMultMod(float modVal)
        {
            var old = Value;
            _multMod += modVal;
            OnChange?.Invoke(Value, Value - old);
        }

        public void ResetMods()
        {
            var old = Value;
            _plusMod = 0f;
            _multMod = 1f;
            OnChange?.Invoke(Value, Value - old);
        }

        public void ResetTo(float newVal)
        {
            var old = Value;
            _baseValue = newVal;
            _plusMod = 0f;
            _multMod = 1f;
            OnChange?.Invoke(Value, Value - old);
        }
    }
}