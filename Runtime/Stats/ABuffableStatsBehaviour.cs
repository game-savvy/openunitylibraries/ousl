using System;
using GameSavvy.OUSL.BuffSystem;
using UnityEngine;

namespace GameSavvy.OUSL.StatSystem
{
    public abstract class ABuffableStatsBehaviour<T> : MonoBehaviour, IBuffable<StatsController<T>> where T : Enum
    {
        [SerializeField]
        protected StatsControllerSerializable<T> _stats;

        [SerializeField]
        protected BuffController<StatsController<T>> _buffController;

        protected virtual void Awake()
        {
            _stats.PopulateStatsController();
            _buffController = new(_stats.StatsController);
        }

        protected virtual void Update()
        {
            _buffController.Update(Time.deltaTime);
        }

        public bool ApplyBuff(ABuffConfig<StatsController<T>> buff, StatsController<T> applier) => _buffController.ApplyBuff(buff, applier);

        public bool ApplyBuff(ABuff<StatsController<T>> buff) => _buffController.ApplyBuff(buff);

        public bool CleanseBuff(ABuffConfig<StatsController<T>> buff, StatsController<T> applier) => _buffController.CleanseBuff(buff, applier);

        public bool CleanseBuff(ABuff<StatsController<T>> buff) => _buffController.CleanseBuff(buff);

        public void ClearAllBuffs() => _buffController.ClearAllActiveBuffs();

        public void CleanseAllBuffs() => _buffController.CleanseAllBuffs();
    }
}