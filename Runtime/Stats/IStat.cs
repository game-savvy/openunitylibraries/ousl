using System;

namespace GameSavvy.OUSL.StatSystem
{
    public interface IStat
    {
        event Action<float, float> OnChange;

        float Value { get; set; }

        float PlusMod { get; }
        void SetPlusMod(float modVal);
        void AddPlusMod(float modVal);

        float MultMod { get; }
        void SetMultMod(float modVal);
        void AddMultMod(float modVal);

        void ResetMods();

        void NotifyObservers();
    }
}