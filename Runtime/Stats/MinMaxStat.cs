using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.StatSystem
{
    [HideReferenceObjectPicker]
    [Serializable]
    public class MinMaxStat : IStat
    {
        public event Action<float, float> OnChange;

        [BoxGroup("__MINMAX_STAT__", ShowLabel = false)]
        [LabelWidth(100)]
        [PropertyOrder(-1)]
        [SerializeField]
        private float _value = 100f;

        /// <summary>
        /// Gets the value of the stat after all modifiers have been applied.
        /// Sets the new current value of the stat.
        /// </summary>
        public float Value
        {
            get => _value;
            set
            {
                var newVal = Mathf.Clamp(value, _minValue, MaxValue);
                var delta = newVal - _value;
                _value = newVal;
                OnChange?.Invoke(_value, delta);
            }
        }

        [FoldoutGroup("__MINMAX_STAT__/Show")]
        [LabelWidth(100)]
        [PropertyOrder(-1)]
        [ShowInInspector]
        public float Percent => Mathf.Clamp01((_value - _minValue) / (MaxValue - _minValue));

        [FoldoutGroup("__MINMAX_STAT__/Show")]
        [LabelWidth(100)]
        [PropertyOrder(0)]
        [SerializeField]
        private float _minValue = 0f;
        public float MinValue => _minValue;

        [FoldoutGroup("__MINMAX_STAT__/Show")]
        [LabelWidth(100)]
        [PropertyOrder(0)]
        [SerializeField]
        private SimpleStat _max;

        public float MaxValue => _max.Value < _minValue ? _minValue : _max.Value;
        public float PlusMod => _max.PlusMod;
        public float MultMod => _max.MultMod;


        [FoldoutGroup("__MINMAX_STAT__/Show")]
        [Button]
        public void NotifyObservers() => OnChange?.Invoke(Value, 0f);


        public MinMaxStat(float value = 100f, float min = 0f, float max = 100f, float plusMod = 0f, float multMod = 1f)
        {
            if (min > max) (min, max) = (max, min);
            _minValue = min;
            _max = new(max, plusMod, multMod);
            _value = value;
            OnChange = null;
            Value = value;
        }


        public void SetPlusMod(float modVal) => ChangeValue(modVal, _max.SetPlusMod);

        public void AddPlusMod(float modVal) => ChangeValue(modVal, _max.AddPlusMod);

        public void SetMultMod(float modVal) => ChangeValue(modVal, _max.SetMultMod);

        public void AddMultMod(float modVal) => ChangeValue(modVal, _max.AddMultMod);

        public void ResetMods() => ChangeValue(0f, (_) => _max.ResetMods());

        private void ChangeValue(float modVal, Action<float> function)
        {
            var oldMax = MaxValue;
            function(modVal);

            var delta = MaxValue - oldMax;
            if (delta > 0f)
            {
                Value += delta;
            }
            else if (delta < 0f)
            {
                Value = _value; // this will Clamp the value to the new max
            }
        }

        public void SetMinValue(float min)
        {
            if (min > MaxValue) min = MaxValue;
            _minValue = min;
            Value = _value;
        }

        public void ResetMaxTo(float newVal)
        {
            var oldMax = MaxValue;
            if (newVal < _minValue) newVal = _minValue;
            _max.ResetTo(newVal);

            var delta = MaxValue - oldMax;
            if (delta > 0f)
            {
                Value += delta;
            }
            else if (delta < 0f)
            {
                Value = _value; // this will Clamp the value to the new max
            }
        }
    }
}