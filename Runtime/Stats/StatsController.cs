using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace GameSavvy.OUSL.StatSystem
{
    [Serializable]
    public class StatsController<TEnum> : IStatsController<TEnum> where TEnum : Enum
    {
        [DictionaryDrawerSettings(KeyLabel = "StatType", ValueLabel = "SimpleStat", DisplayMode = DictionaryDisplayOptions.OneLine)]
        [ShowInInspector]
        public Dictionary<TEnum, SimpleStat> _simpleStats;

        [DictionaryDrawerSettings(KeyLabel = "StatType", ValueLabel = "MinMaxStat", DisplayMode = DictionaryDisplayOptions.OneLine)]
        [ShowInInspector]
        public Dictionary<TEnum, MinMaxStat> _minMaxStats;

        public StatsController(int simpleCapacity = 0, int minMaxCapacity = 0)
        {
            _simpleStats = new(simpleCapacity);
            _minMaxStats = new(minMaxCapacity);
        }


        public int SimpleStatsCount => _simpleStats?.Count ?? 0;
        public int MinMaxStatsCount => _minMaxStats?.Count ?? 0;


        public int StatsCount => _simpleStats.Count + _minMaxStats.Count;

        public bool TryAddStat(TEnum key, SimpleStat stat) => _simpleStats.TryAdd(key, stat);

        public bool TryAddStat(TEnum key, MinMaxStat stat) => _minMaxStats.TryAdd(key, stat);

        public bool TryAddSimpleStat(TEnum key, float value) => _simpleStats.TryAdd(key, new(value));

        public bool TryAddMinMaxStat(TEnum key, float value, float min = 0, float max = 100) => _minMaxStats.TryAdd(key, new(value, min, max));


        public bool TryGetSimpleStat(TEnum key, out SimpleStat stat) => _simpleStats.TryGetValue(key, out stat);

        public bool TryGetMinMaxStat(TEnum key, out MinMaxStat stat) => _minMaxStats.TryGetValue(key, out stat);

        public bool TryGetIStat(TEnum key, out IStat stat)
        {
            if (_simpleStats.TryGetValue(key, out var simpleStat))
            {
                stat = simpleStat;
                return true;
            }

            if (_minMaxStats.TryGetValue(key, out var minMaxStat))
            {
                stat = minMaxStat;
                return true;
            }

            stat = default;
            return false;
        }

        public bool TryGetValue(TEnum key, out float val)
        {
            if (_simpleStats.TryGetValue(key, out var simpleStat))
            {
                val = simpleStat.Value;
                return true;
            }

            if (_minMaxStats.TryGetValue(key, out var minMaxStat))
            {
                val = minMaxStat.Value;
                return true;
            }

            val = default;
            return false;
        }


        public bool TryRemoveSimpleStat(TEnum key) => _simpleStats.Remove(key);

        public bool TryRemoveMinMaxStat(TEnum key) => _minMaxStats.Remove(key);

        public bool TryRemoveStat(TEnum key)
        {
            if (_simpleStats.ContainsKey(key))
            {
                return _simpleStats.Remove(key);
            }

            if (_minMaxStats.ContainsKey(key))
            {
                return _minMaxStats.Remove(key);
            }

            return false;
        }


        public void ClearStats()
        {
            _simpleStats.Clear();
            _minMaxStats.Clear();
        }
    }
}