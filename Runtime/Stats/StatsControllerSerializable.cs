using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.StatSystem
{
    [Serializable]
    public class StatsControllerSerializable<T> where T : Enum
    {
        [Serializable]
        protected class SimpleStatKV
        {
            public T Key = default;

            [InlineProperty]
            [HideLabel]
            public SimpleStat Value = new(100f, 0f, 1f);
        }

        [Serializable]
        protected class MinMaxStatKV
        {
            public T Key = default;

            [InlineProperty]
            [HideLabel]
            public MinMaxStat Value = new(100f, 0f, 100f, 0f, 1f);
        }

        [HideInPlayMode]
        [SerializeField]
        protected SimpleStatKV[] _simpleStats;

        [HideInPlayMode]
        [SerializeField]
        protected MinMaxStatKV[] _minMaxStats;

        [HideInEditorMode]
        [InlineProperty]
        [HideLabel]
        [SerializeField]
        protected StatsController<T> _statsController;
        public StatsController<T> StatsController => _statsController;

        public StatsControllerSerializable()
        {
            PopulateStatsController();
        }

        public void PopulateStatsController()
        {
            if (_simpleStats == null || _minMaxStats == null) return;

            _statsController = new(_simpleStats.Length, _minMaxStats.Length);
            foreach (var stat in _simpleStats)
            {
                _statsController.TryAddStat(stat.Key, stat.Value);
            }
            foreach (var stat in _minMaxStats)
            {
                _statsController.TryAddStat(stat.Key, stat.Value);
            }
        }
    }
}