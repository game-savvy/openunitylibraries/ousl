# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).



## [0.3.0] - 2024-01-10
### Added
- Stats, SimpleStat, MinMaxStat
- BuffSystem
- Sample Scene: Containing Examples on how to use the Library.
- Unity Editor Tests

### Changed
- Updated classes to follow naming conventions


## [0.1.2] - 2020-05-19
### Added
- This Changelog.
- Empty Editor and Playmode Tests.
- Sample Scene: Containing Examples on how to use the Library.

### Changed
- Restructured the project in order to be UnityPackages Complient.
