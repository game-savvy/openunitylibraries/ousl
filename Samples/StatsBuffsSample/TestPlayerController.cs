using GameSavvy.OUSL.BuffSystem;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameSavvy.OUSL.Samples.StatsBuffsSample
{
    public class TestPlayerController : MonoBehaviour, IBuffable<TestPlayerStats>
    {
        [BoxGroup("Player Stats")]
        [HideLabel]
        [SerializeField]
        private TestPlayerStats _stats;

        [InlineEditor]
        [SerializeField]
        private MoveSpeedPlusModPassiveBuffCfg _moveSpeedBoost1PassiveCfg;

        [InlineEditor]
        [SerializeField]
        private MoveSpeedPlusModPassiveBuffCfg _moveSpeedBoost2PassiveCfg;

        [InlineEditor]
        [SerializeField]
        private MoveSpeedMultModTimedBuffCfg _moveSpeedBoostTimedBuffCfg1;

        [InlineEditor]
        [SerializeField]
        private MoveSpeedMultModTimedBuffCfg _moveSpeedBoostTimedBuffCfg2;

        [InlineEditor]
        [SerializeField]
        private HealthValueOverTimeBuffCfg _healthPlusModTimedBuffCfg1;

        [InlineEditor]
        [SerializeField]
        private HealthValueOverTimeBuffCfg _healthPlusModTimedBuffCfg2;

        [SerializeField]
        private BuffController<TestPlayerStats> _buffController;

        public bool ApplyBuff(ABuffConfig<TestPlayerStats> buff, TestPlayerStats applier) => _buffController.ApplyBuff(buff, applier);
        public bool ApplyBuff(ABuff<TestPlayerStats> buff) => _buffController.ApplyBuff(buff);

        public bool CleanseBuff(ABuffConfig<TestPlayerStats> buff, TestPlayerStats applier) => _buffController.CleanseBuff(buff, applier);
        public bool CleanseBuff(ABuff<TestPlayerStats> buff) => _buffController.CleanseBuff(buff);

        private void Start()
        {
            _buffController = new(_stats);
        }

        private void Update()
        {
            _buffController.Update(Time.deltaTime);

            if (Keyboard.current.digit0Key.wasPressedThisFrame)
            {
                _buffController.CleanseAllBuffs();
            }
            else if (Keyboard.current.digit1Key.wasPressedThisFrame)
            {
                ApplyBuff(_moveSpeedBoost1PassiveCfg, _stats);
            }
            else if (Keyboard.current.digit2Key.wasPressedThisFrame)
            {
                ApplyBuff(_moveSpeedBoost2PassiveCfg, _stats);
            }
            else if (Keyboard.current.digit3Key.wasPressedThisFrame)
            {
                ApplyBuff(_moveSpeedBoostTimedBuffCfg1, _stats);
            }
            else if (Keyboard.current.digit4Key.wasPressedThisFrame)
            {
                ApplyBuff(_moveSpeedBoostTimedBuffCfg2, _stats);
            }
            else if (Keyboard.current.digit5Key.wasPressedThisFrame)
            {
                ApplyBuff(_healthPlusModTimedBuffCfg1, _stats);
            }
            else if (Keyboard.current.digit6Key.wasPressedThisFrame)
            {
                ApplyBuff(_healthPlusModTimedBuffCfg2, _stats);
            }
        }

    }
}