using System;

namespace GameSavvy.OUSL.Samples.StatsBuffsSample
{
    [Serializable]
    public enum FakeStatType
    {
        None,           // never to be created/instantiated
        Speed,          // SimpleStat
        PhysicalDamage, // SimpleStat
        MagicalDamage,  // SimpleStat
        Health,         // MinMaxStat
        Mana,           // MinMaxStat
        PhysicalArmor   // MinMaxStat
    }
}