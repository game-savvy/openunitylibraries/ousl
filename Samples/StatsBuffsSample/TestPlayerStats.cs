﻿using System;
using GameSavvy.OUSL.StatSystem;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.Samples.StatsBuffsSample
{
    [Serializable]
    public class TestPlayerStats
    {
        [InlineProperty]
        [LabelWidth(120)]
        [SerializeField]
        private MinMaxStat _health = new(100f, 0f, 100f);
        public MinMaxStat HealthStat => _health;

        [InlineProperty]
        [LabelWidth(120)]
        [SerializeField]
        private MinMaxStat _mana = new(100f, 0f, 100f);
        public MinMaxStat ManaStat => _mana;

        [InlineProperty]
        [LabelWidth(120)]
        [SerializeField]
        private SimpleStat _physicalAttack = new(12f, 0f, 1f);
        public SimpleStat PhysicalAttackStat => _physicalAttack;

        [InlineProperty]
        [LabelWidth(120)]
        [SerializeField]
        private SimpleStat _movementSpeed = new(8f, 0f, 1f);
        public SimpleStat MovementSpeedStat => _movementSpeed;
    }
}