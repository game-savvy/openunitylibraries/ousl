using GameSavvy.OUSL.BuffSystem;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthValueOverTimeBuffCfg", menuName = "Buffs/HealthValueOverTimeBuffCfg")]
public class HealthValueOverTimeBuffCfg : ABuffConfig<TestPlayerStats>
{
    [SerializeField]
    private float _healthPerTick = 0f;
    public float HealthPerTick => _healthPerTick;

    [ShowInInspector]
    public float HPS => TotalHealth / _duration;

    [ShowInInspector]
    public float TotalHealth => _healthPerTick * _targetTickCount;


    public override ABuff<TestPlayerStats> GenerateBuff(TestPlayerStats receiver, TestPlayerStats applier)
    {
        return new HealthValueOverTimeBuff(this, receiver, applier);
    }
}