using UnityEngine;
using GameSavvy.OUSL.BuffSystem;

[CreateAssetMenu(fileName = "MoveSpeedPercentTimedBuffCfg", menuName = "Buffs/MoveSpeedPercentTimedBuffConfig")]
public class MoveSpeedMultModTimedBuffCfg : ABuffConfig<TestPlayerStats>
{
    [SerializeField]
    private float _moveSpeedMultAmount = -0.2f;
    public float MoveSpeedMultAmount => _moveSpeedMultAmount;

    public override ABuff<TestPlayerStats> GenerateBuff(TestPlayerStats receiver, TestPlayerStats applier)
    {
        return new MoveSpeedMultModTimedBuff(this, receiver, applier);
    }
}