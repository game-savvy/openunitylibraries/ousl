using UnityEngine;
using GameSavvy.OUSL.BuffSystem;

[CreateAssetMenu(fileName = "MoveSpeedBoostPassiveCfg", menuName = "Buffs/MoveSpeedBoostPassiveConfig")]
public class MoveSpeedPlusModPassiveBuffCfg : ABuffConfig<TestPlayerStats>
{
    [SerializeField]
    private float _movePlusModAmout = -1f;
    public float MovePlusModAmout => _movePlusModAmout;

    public override ABuff<TestPlayerStats> GenerateBuff(TestPlayerStats receiver, TestPlayerStats applier)
    {
        return new MoveSpeedPlusModPassiveBuff(this, receiver, applier);
    }
}