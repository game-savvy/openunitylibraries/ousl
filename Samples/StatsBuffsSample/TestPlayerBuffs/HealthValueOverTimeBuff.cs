using GameSavvy.OUSL.BuffSystem;

public class HealthValueOverTimeBuff : ABuff<TestPlayerStats>
{
    private readonly HealthValueOverTimeBuffCfg _config;

    public HealthValueOverTimeBuff(HealthValueOverTimeBuffCfg config, TestPlayerStats receiver, TestPlayerStats applier) :
        base(config, receiver, applier)
    {
        _config = config;
    }

    protected override void OnTimerTick()
    {
        Receiver.HealthStat.Value += _config.HealthPerTick;
    }
}