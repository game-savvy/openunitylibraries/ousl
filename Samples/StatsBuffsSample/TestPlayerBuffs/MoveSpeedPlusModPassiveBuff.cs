using GameSavvy.OUSL.BuffSystem;

public class MoveSpeedPlusModPassiveBuff : ABuff<TestPlayerStats>
{
    private readonly MoveSpeedPlusModPassiveBuffCfg _config;

    public MoveSpeedPlusModPassiveBuff(MoveSpeedPlusModPassiveBuffCfg config, TestPlayerStats receiver, TestPlayerStats applier) :
        base(config, receiver, applier)
    {
        _config = config;
    }

    protected override void OnApply()
    {
        Receiver.MovementSpeedStat.AddPlusMod(_config.MovePlusModAmout);
    }

    protected override void OnRemove()
    {
        Receiver.MovementSpeedStat.AddPlusMod(-1f * _config.MovePlusModAmout);
    }
}