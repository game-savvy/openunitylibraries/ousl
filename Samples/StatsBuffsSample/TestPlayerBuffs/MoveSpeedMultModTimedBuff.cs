using GameSavvy.OUSL.BuffSystem;

public class MoveSpeedMultModTimedBuff : ABuff<TestPlayerStats>
{
    private readonly MoveSpeedMultModTimedBuffCfg _config;

    public MoveSpeedMultModTimedBuff(MoveSpeedMultModTimedBuffCfg config, TestPlayerStats receiver, TestPlayerStats applier) :
        base(config, receiver, applier)
    {
        _config = config;
    }

    protected override void OnApply()
    {
        Receiver.MovementSpeedStat.AddMultMod(_config.MoveSpeedMultAmount);
    }

    protected override void OnRemove()
    {
        Receiver.MovementSpeedStat.AddMultMod(-1f * _config.MoveSpeedMultAmount);
    }
}