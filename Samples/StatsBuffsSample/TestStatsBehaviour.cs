using GameSavvy.OUSL.StatSystem;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameSavvy.OUSL.Samples.StatsBuffsSample
{
    public class TestStatsBehaviour : ABuffableStatsBehaviour<FakeStatType>
    {
        [Space(10)]
        [Header("More Stats")]
        [LabelWidth(100)]
        [InlineProperty]
        [SerializeField]
        protected SimpleStat _simpleStat1 = new(100f, 0f, 1f);

        [LabelWidth(100)]
        [InlineProperty]
        [SerializeField]
        protected MinMaxStat _minMaxStat1 = new(100f, 0f, 100f, 0f, 1f);
    }
}